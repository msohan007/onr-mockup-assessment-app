import React, { Component } from 'react';
import Menu from "../../Assets/menu.png";
import Print from "../../Assets/print.png";
import QuestionMark from "../../Assets/question-mark.png";
import Forward from "../../Assets/forward.png";



function Header() {
  
  return (
      <div className="menubar">
      <h6 className="menu-heading  float-left">Diagnostic Tool</h6>
          <ul className="float-right">
              <a href="#"><img className="icon" src={Menu} alt="Menu logo"/></a>
              <a href="#"><img className="icon" src={Print} alt="Print logo"/></a>
              <a href="#"><img className="icon" src={QuestionMark} alt="QuestionMark logo"/></a>
              <a href="#"><img className="icon" src={Forward} alt="Forward logo"/></a>
          </ul>
          <p className="username float-right">Logged in as General User</p>
      </div>
  );
}

export default Header;
