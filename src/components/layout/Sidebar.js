
import React, { Component,useState } from 'react';
import Pin from "../../Assets/pin.png";
import Bargraph from "../../Assets/bargraph.png";
import Mail from "../../Assets/email.png";
import Tools from "../../Assets/tools.png";
import Report from "../../Assets/report.png";
import { BrowserRouter as router, Router, Link } from 'react-router-dom'
import PerformanceManagement from '../performanceManagement/PerformanceManagement'


//Functional Component
function Sidebar() {
    const [state,setState]=useState(false);
  let url="https://thinkster.io/tutorials/react-router/creating-links";
  let element=<a href={url}></a>;
    return (
        <div className="sidenav">
               <a href="#"><img className="icon" src={Pin} alt="BigCo Inc. logo"/></a>
               <a href="#"><img className="icon" src={Bargraph} alt="BigCo Inc. logo"/></a>
               <a href="#"><img className="icon" src={Mail} alt="BigCo Inc. logo"/></a>
               <a href="#"><img className="icon" src={Tools} alt="BigCo Inc. logo"/></a>
               <a href="#"><img className="icon" src={Report} alt="BigCo Inc. logo"/></a>
        </div>
    );
}

export default Sidebar;
