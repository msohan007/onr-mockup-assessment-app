
import React, { Component } from 'react';
import * as d3 from 'd3'
import './donut-pie.css';




export default function DonutPie(dataDetails) {
        // console.log(dataDetails, 'ooo');
        const width = 200
        const height = 200
        //var margin = 40 

        const radius = 50;
        const innerRadius = 35;

        //********** To colored the pie segments *****************/
        //const color=d3.scaleOrdinal(['green','yellow','grey','red']);
        var colors = d3.scaleOrdinal(d3.schemeDark2);


        var g = d3.select("#my_dataviz")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .style("position", "relative")
            .style("left", "770")
            .style("top", "70")
            

        //**********Data into pie- generator function *****************/        
        var data = d3.pie().sort(null).value(function (d) { return d.score  })(dataDetails);


        //********** Arc- generator function *****************/
        const segments = d3.arc().outerRadius(radius).innerRadius(innerRadius);
        //console.log(path);

        var sections = g.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
            .selectAll("path").data(data);

        sections.enter().append("path").attr("d", segments)
            .attr('fill', d => colors(d.data.score));


    return (
         <div id="my_dataviz"></div>

    );


}
//export default DonutPie;




/****** Show the label ***********
  var content =d3.select("g").selectAll("text").data(data);
  content.enter().append("text").classed("inside",true).each(function (d){
      var center=segments.centroid(d);
      d3.select(this).attr("x",center[0]).attr("y",center[1])
      .text(d.data.score)
  })
*/

/****** To add a legend **********
var legends = g.append("g").attr("tranform", "translate(200,10)")
.selectAll(".legends").data(data);
var legend= legends.enter().append("g").classed("legends",true).attr('transform',function (d,i){return "translate(0," +(i+1)+10+")";});
legend.append("rect").attr("width",20).attr("height",20).attr("fill", d=>colors(d.data.score));
legend.append("text").classed("label",true).text(function (d){console.log(d.data.name);return d.data.name;})
.attr("fill",d=>colors(d.data.score))
.attr("x",31).attr("y",18)
*/