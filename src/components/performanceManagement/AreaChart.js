
/*
import './diagnostic-tools.css';
import React, { Component, useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as d3 from 'd3';
import DonutPie from './donutPie';
import { areaData } from "../../data/data.json";
import { select, axisBottom, extent, scaleLinear, scaleTime, axisLeft, line, area, curveBasis, json } from 'd3';



const svg = select("svg");

const width = +svg.attr('width');
const height = +svg.attr('height');

const render = data => {
    const title = "Area Chart";

    const xValue = d => d.date;
    const xAxisLabel = 'Month';

    const yValue = d => d.score;
    const circleRadius = 6;
    const yAxisLabel = 'Score';

    const margin = { top: 60, right: 40, bottom: 88, left: 105 };

    const innerWidth = width - margin.left - margin.right;
    const innerHeight = height - margin.top - margin.bottom;

    const xScale = scaleTime()
        .domain(extent(data, xValue))
        .range([0, innerWidth])
        .nice();

    const yScale = scaleLinear()
        .domain(extent(data, yValue))
        .range([innerHeight, 0])
        .nice();

    const g = svg.append('g')
        .attr('transform', `translate(${margin.left},{margin.top})`);

    const xAxis = axisBottom(xScale)
        .tickSize(-innerHeight)
        .tickPadding(15);

    const yAxis = axisLeft(yScale)
        .tickSize(-innerWidth)
        .tickPadding(10);

    const yAxisG = g.append('g').call(yAxis);
    yAxisG.selectAll('.domain').remove();

    yAxisG.append('text')
        .attr('class', 'axis-label')
        .attr('y', -60)
        .attr('x', -innerHeight / 2)
        .attr('fill', 'black')
        .attr('transform', `rotate(-90)`)
        .attr('text-anchor', 'middle')
        .text(yAxisLabel);

    const areaGenerator = area()
        .x(d => xScale(xValue(d)))
        .y(d => yScale(yValue(d)))
        .curve(curveBasis);

    g.append('path')
        .attr('class', 'line-path')
        .attr('d', areaGenerator(data));

    g.append('text')
        .attr('class', 'title')
        .attr('y', -10)
        .text(title);

}//render


var data= areaData.forEach(d => {
        d.score = +d.score;
        d.date = new Date(d.date)

    });
    render(data);
    //console.log(data);
*/


