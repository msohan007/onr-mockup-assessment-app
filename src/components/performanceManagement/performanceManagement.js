import React, { Component } from 'react';
import DiagnosticTool from "../../Assets/diagnostic-tool.png";
import PushPin from "../../Assets/push-pin.png";
import DiagnosticTools from "./Diagnostic-tools";
import DonutPie  from './donutPie';


function PerformanceManagement() {

    return (
        <div className="header">
            <p>PERFORMANCE MANAGEMENT</p>
            <div className="page-header">
                <p>
                    <img className="icon" src={DiagnosticTool} alt="Diagnostic-tool logo"/> Diagnostic Tool
                </p>
                <img src={PushPin} alt="PinLogo"/>
            </div>

            <div>
                <DiagnosticTools/>
                
            </div>
        </div>
    );
}


export default PerformanceManagement;
