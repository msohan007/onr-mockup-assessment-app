// import React, { Component } from 'react'
import './diagnostic-tools.css';
import React, { Component, useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import * as d3 from 'd3'
import DonutPie from './donutPie';
import AreaChart from './AreaChart';
import { gaugeData } from "../../data/data.json";




export class DiagnosticTools extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             dataSet : []
        }
    
    }

    componentDidMount() {
        return this.setState({
            dataSet : gaugeData.map((dataDetails) => {
                //console.log(dataset);
                return dataDetails;
                   
                })
        }) 

      }
    render() {
        return (
            <div className="container-fluid" style={{ padding: "1% 8%" }}>
                <div className="row" >
                    <div className="col-md-6" style={{ padding: "0px" }}>
                        <h6>Filters</h6>
                        <div className="filter col-md-5" style={{ margin: "0px", paddingLeft: "1%" }}>
                            <div className="ui checkbox" style={{ margin: "0px", paddingLeft: "1%" }}>
                                <input type="checkbox" /><label > All CQA Results</label><br></br>
                                <input type="checkbox" /><label > CQAs with Closed Loop</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="grid-container">
                                {
                                  //this.state.dataSet,
                                    gaugeData.map((dataDetails, index) => {                                       
                                        
                                        return (
                                            <div className="grid-item" key={index}>
                                                <p>{dataDetails.name}</p><br/>
                                                <p>{dataDetails.score}%</p><br/>
                                                {/*<div> {DonutPie(gaugeData)} </div>*/}
                                                <p>Sample: {dataDetails.sample}</p>
                                            </div>
                                        );
                                    })
    
                                }
                            </div>
                        </div>
                    </div>
                    <div className="trend col-md-6 " style={{ padding: "0%" }}>
                        <div className="nav " style={{ padding: "0%", margin: "0%" }}>
                            <p style={{ paddingRight: "3%", fontFamily: "Helvetica", fontSize: "16px", margin: "0%" }}>QUALITY SCORE TREND</p>
                            <button className="btn btn-primary" type="button">Day</button>
                            <button className="btn btn-primary" type="button">Week</button>
                            <button className="btn btn-primary active" type="button">Month</button>
                            <button className="btn btn-primary" type="button">Quater</button>
                            <button className="btn btn-primary" type="button">Half</button>
                            <button className="btn btn-primary" type="button">Year</button>
                        </div>
                        {/*<AreaChart/>*/}
                        {/*<GetData/>*/}
                      
    
                    </div>
                </div>
    
            </div>
    
        );
    }
}

export default DiagnosticTools
