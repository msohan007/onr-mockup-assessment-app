import React, { Component } from 'react';
import './App.css';
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';
import Sidebar from './components/layout/Sidebar';
import PerformanceManagement from './components/performanceManagement/PerformanceManagement';
//import { BrowserRouter as router, Router, Link } from 'react-router-dom'


//Functional Component
function App() {
    return (
        <div className="body">
            <Sidebar />
            <Header />
            <PerformanceManagement />
            <Footer />
        </div>
    );
}

export default App;
